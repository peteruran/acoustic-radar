#!/usr/bin/python3
import numpy as np
import matplotlib.pyplot as plt
import os
import time
from scipy import signal
from multiprocessing import Process, Pool

np.set_printoptions(threshold=np.inf) # For printing ndarrays correctly
path = os.path.dirname(os.path.realpath(__file__)) # For correctly finding imput files

## Configuration
file = "klapp1.bin"
output = "output.txt"
channels = 5
audio_channels = 3 # Drops channels outside
conversion_coeff = 1.65 / 4095 # volt per bit

## Imoprts ADC data from bin file. First value is sample period, followed by actual data
def raspi_import(file, channels):
	with open(file, 'rb') as fid:
		sample_period = np.fromfile(fid, dtype=np.float64, count=1)[0] * 1e-6 # sample period in seconds
		data = np.fromfile(fid, dtype=np.uint16)
	data =  data*conversion_coeff
	data = data.reshape((-1,channels)).T ## Reshaping matrix with one column per row
	return sample_period, data

## Detect angle based on delays between microphones in array
def detect_angle(delay_21, delay_31, delay_32):
	return np.arctan2( np.sqrt(3) * (delay_21 + delay_31), (delay_21 - delay_31 - 2*delay_32) )

## Worker function for calculating correlation and delay. Can be run in parallell
def correlation_worker(a, v):
	corr = signal.correlate(a, v, mode="full", method='fft')
	delay = corr.argmax() - a.size
	return (corr, delay)


## Fetching data from file
print("Fetching data...")
sample_period, data = raspi_import(path + '/' + file, channels)

print('Sample period is ' + str(sample_period))

#print(data.mean(axis=1))
data = (data.T - data.mean(axis=1)).T
data = data[:3]
data = np.delete(data, (0,100), 1) # ingrore first 100 samples

## Starting corrolation. Adding clock for benchmarking
print("Corrolating...")
start_time = time.clock()

## Running correlation_worker as a pool of processes
work = (
	[data[1], data[0]],
	[data[2], data[0]],
	[data[2], data[1]]
	)
pool = Pool(3)
results = pool.starmap(correlation_worker, work)
pool.close()
pool.join()

end_time = time.clock()
print("CONV. TIME: ", (end_time-start_time)*1000, ' ms')

corr_21, delay_21 = results[0]
corr_31, delay_31 = results[1]
corr_32, delay_32 = results[2]

print('DELAY 2-1: ', delay_21)
print('DELAY 3-1: ', delay_31)
print('DELAY 3-2: ', delay_32)


angle = detect_angle(delay_21, delay_31, delay_32)
if angle < 0: # correction to arctan
	angle += 2*np.pi

print('ANGLE:', np.degrees(angle))

## write angle to file. Create file of not exist
with open(output, 'a+') as fid:
	fid.write(str( np.degrees(angle) ) + '\n')

print('Done!')



'''
print("Plotting...")

# Input signals
plt.subplot(3,2,1)
#plt.plot(np.arange(0,data.size,1), data[0], linestyle='-', marker='.', color='b')
#plt.plot(np.arange(0,data[0].size,1), data[0], linestyle='-', marker='.', color='b')
plt.plot(data[0].T)

plt.subplot(3,2,3)
plt.plot(data[1].T)

plt.subplot(3,2,5)
plt.plot(data[2].T)

# Correlations
plt.subplot(3,2,2)
plt.plot(corr_21)

plt.subplot(3,2,4)
plt.plot(corr_31)

plt.subplot(3,2,6)
plt.plot(corr_32)


#plt.plot(data.T)
plt.show()
'''
